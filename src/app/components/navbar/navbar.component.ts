import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { AuthentificationService } from 'src/app/services/authentification.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public authentificationService: AuthentificationService,
    private cookieService: CookieService) { }

  ngOnInit(): void {
  }

  //Function used to logout
  userLogout(){
    this.authentificationService.logout().subscribe(
      res => {
        this.cookieService.deleteAll();
      },
      err => {
        console.log(err);
      }
    );
  }

}
