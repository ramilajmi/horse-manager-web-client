import { Component, OnInit } from '@angular/core';
import { HorsesserviceService } from 'src/app/services/horsesservice.service';
import { Horse } from 'src/app/models/horse.model';
import { HorsesComponent } from '../horses/horses.component';

 //use jquery with this declaration
 declare var $: any;

@Component({
  selector: 'app-horse-modal',
  templateUrl: './horse-modal.component.html',
  styleUrls: ['./horse-modal.component.css']
})
export class HorseModalComponent implements OnInit {

  horse = new Horse;

  //Submitted state after send data to API
  submitted = false;

  constructor(private service: HorsesserviceService,
    private parent: HorsesComponent) { }

  ngOnInit(): void {
  }

  //Display modal
  showModal():void {
    $("#myModal").modal('show');
  }

  //Hide modal 
  hideModal():void {  
    $("#myModal").modal('hide');
  }

  //Create horse function in API
  createHorse(): void {
    const data = {
      name: this.horse.name,
      colour: this.horse.colour,
      speed: this.horse.speed,
      breed: this.horse.breed,
      image: this.horse.image
    };

    // Create horse API ws
    this.service.createHorse(data)
    .subscribe(
      res => {
        console.log("res");
        console.log(res);
        this.submitted = true;
        this.parent.fetchHorses();
        this.hideModal();
      },
      err => {
        console.log("err");
        console.log(err);
        this.submitted = false;
        this.hideModal();
      }
    );
  }
}
