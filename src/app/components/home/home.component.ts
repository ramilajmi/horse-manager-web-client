import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/app/services/authentification.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authentificationService: AuthentificationService,
    private router: Router) { }

  ngOnInit(): void {
    //if user is not authenticate then autoredirect to /login page
    if(!this.authentificationService.isAuthenticated()){
      this.router.navigate(["login"]);
    }
  }
}
