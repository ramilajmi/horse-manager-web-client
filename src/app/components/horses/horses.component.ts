import { Component, OnInit } from '@angular/core';
import { Horse } from 'src/app/models/horse.model';
import { HorsesserviceService } from 'src/app/services/horsesservice.service';
import { ActivatedRoute } from '@angular/router';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-horses',
  templateUrl: './horses.component.html',
  styleUrls: ['./horses.component.css']
})
export class HorsesComponent implements OnInit {
  
  horsesTab: Horse[] = [];

  constructor(private activatedRoute: ActivatedRoute,
    private service: HorsesserviceService,
    private _snackBar: MatSnackBar) { }    

  ngOnInit(): void {  
    this.fetchHorses();
  }

  // Function used to fetch all horses from API
  fetchHorses() {
    return this.service.getHorses().subscribe((response: any) => {
      this.horsesTab = response;
    });
  }
  
  // Show modal
  showModal(): void{
    console.log("Display modal");
  }

  //Show Toast
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 1000,
      verticalPosition: 'bottom',
      horizontalPosition: 'end'
    });
  }

  
  
}
