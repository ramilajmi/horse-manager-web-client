import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/app/services/authentification.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user = {
    firstname: '',
    lastname: '',
    email: '',
    username: '',
    password: '',
  };

  constructor(private authentificationService: AuthentificationService,
    private router: Router) { }

  ngOnInit(): void {
    //if user is authenticate then autoredirect to /home
    if(this.authentificationService.isAuthenticated()){
      this.router.navigate(["home"]);
    }
  }

  //Function used to register user to horse manager app
  userRegister(): void {
    const data = {
      firstname: this.user.firstname,
      lastname: this.user.lastname,
      email: this.user.email,
      username: this.user.username,
      password: this.user.password
    };

    //ws call register function from auth service
    this.authentificationService.register(data)
    .subscribe(
      res => {
        this.router.navigate(["login"]);
      },
      err => {
        console.log(err);
      }
    );
  }

}
