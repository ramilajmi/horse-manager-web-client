import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  invalidLogin: boolean | undefined;

  user = {
    username: '',
    password: '',
  };

  constructor(private authentificationService: AuthentificationService,
    private router: Router,
    private cookieService: CookieService) { }

  ngOnInit(): void {
    //if user is authenticate then autoredirect to /home
    if(this.authentificationService.isAuthenticated()){
      this.router.navigate(["home"]);
    }
  }

  //Function used to log user to horse manager app
  userLogin(): void {
    const data = {
      username: this.user.username,
      password: this.user.password
    };

    //ws call login function from auth service
    this.authentificationService.login(data)
    .subscribe(
      res => {
        if(res.type){
          this.cookieService.set('connected_u', data.username);
          this.invalidLogin = false;
          this.router.navigate(["home"]);
        }else{
          this.invalidLogin = true;
        }
      },
      err => {
        console.log(err);
        this.invalidLogin = true;
      }
    );
  }
  
}
