export class Horse {
    name: string="";
    colour: string="";
    speed: number=0;
    breed: string="";
    image: string="";
}
