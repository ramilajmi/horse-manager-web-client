import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

// API url
const baseUrl = 'http://localhost:9000/api/';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  constructor(private http: HttpClient,
    private cookieService: CookieService) { }

  // Function used to check if user is Authentificated or not
  isAuthenticated(): Boolean{
    if(this.cookieService.get('connected_u') != ""){
      return true;
    }else{
      return false;
    }
  }

  // WS function to register user to API
  register(data: any): Observable<any> {
    return this.http.post(baseUrl+'register', data, {
      withCredentials: true
    });
  }

  // WS function to log user to API
  login(data: any): Observable<any> {
    return this.http.post(baseUrl+'auth', data, {
      withCredentials: true
    });
  }

  //WS function to logout user from API
  logout(): Observable<any> {
   this.cookieService.deleteAll();
   return this.http.post(baseUrl+'logout', "", {
    withCredentials: true
   })
  }

}
