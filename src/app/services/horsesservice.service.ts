import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HorsesserviceService {

  //API url
  readonly baseURL = "http://localhost:9000/api/horses"
  
  constructor(private http: HttpClient) { }

  //List all horses from API
  getHorses(): Observable<any> {
    return this.http.get(this.baseURL)
    .pipe(
      retry(1),
      catchError(this.processError)
    )
  }

  //Create horse in API
  createHorse(data: any): Observable<any> {
    return this.http.post(this.baseURL, data, {
      withCredentials: true
    });
  }

  //Catch error from API response
  processError(err :any) {
    let message = '';
    if(err.error instanceof ErrorEvent) {
     message = err.error.message;
    } else {
     message = `Error Code: ${err.status}\nMessage: ${err.message}`;
    }
    console.log(message);
    return throwError(message);
  }

}
