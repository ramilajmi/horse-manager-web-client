import { TestBed } from '@angular/core/testing';

import { HorsesserviceService } from './horsesservice.service';

describe('HorsesserviceService', () => {
  let service: HorsesserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HorsesserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
