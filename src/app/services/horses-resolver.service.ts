import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
 
import { HorsesserviceService } from 'src/app/services/horsesservice.service';
 
@Injectable({
  providedIn: 'root'
})

export class HorsesResolverService implements Resolve<any> {

  constructor(private service: HorsesserviceService) {}
  
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    console.log('Called Get Product in resolver...', route);
    return this.service.getHorses().pipe(
      catchError(error => {
        return of('No data');
      })
    );
  }

}